package alpha.batch.History;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class SaveWinNum {

	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String NURL = "http://www.nlotto.co.kr/common.do?method=getLottoNumber&drwNo=";

	private static SqlSessionFactory sqlMapper = null;

	static {
		try {

			String resource = "config/spring/context-mybatis.xml";
			Reader reader = Resources.getResourceAsReader(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(reader);
		} catch (IOException e) {
			e.printStackTrace();

		}

	}

	public void saveNumber() throws Exception {
		try {
			// DB에 저장 된 Max 회차 구하기
			int nDbMax = getDbMax();
			System.out.println("DbMax = " + nDbMax);
			// 최근 발표 한 Max 회차 구하기
			int nNewMax = getNewMax();
			System.out.println("NewMax = " + nNewMax);

			// DB에 저장 된 Max+1회차 부터 최근 발표 한 Max 회차까지 번호 저장

			List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();

			for (int i = nDbMax + 1; i <= nNewMax; i++) {
				list.add(getNumber(i));
			}

			SqlSession session = sqlMapper.openSession();

			for (int j = 0; j < list.size(); j++) {

/*				System.out.println(list.get(j).get("drwNo"));
				System.out.println(list.get(j).get("N0"));
				System.out.println(list.get(j).get("N1"));
				System.out.println(list.get(j).get("N2"));
				System.out.println(list.get(j).get("N3"));
				System.out.println(list.get(j).get("N4"));
				System.out.println(list.get(j).get("N5"));*/

				int nResult = session.insert("sample.insertWinNum", list.get(j));
//				System.out.println("insert Result = " + nResult);
				session.commit();

			}

			session.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private int getNewMax() throws Exception {

		String url = NURL;

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");
		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(response.toString());

		int i = 0;
		i = Integer.parseInt(jsonObject.get("drwNo").toString());

		return i;
	}

	private int getDbMax() {

		int i = 0;

		try {

			SqlSession session = sqlMapper.openSession();

			if (session.selectOne("sample.selectDbMax") != null) {
				i = session.selectOne("sample.selectDbMax");
			}

			session.close();
		} catch (Exception e) {
			e.printStackTrace();

		}
		return i;

	}

	private void showSelect() {

		try {

			SqlSession session = sqlMapper.openSession();

			List<Map<String, Object>> list = session.selectList("sample.selectLottoList");

			for (int i = 0; i < list.size(); i++) {
				System.out.println(list.get(i).get("host_city"));

			}

			// session.commit();
			session.close();

			System.out.println("the end");

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	// HTTP GET request
	private HashMap<String, Object> getNumber(int num) throws Exception {

		String url = NURL + num;
		HashMap<String, Object> lv_hs = new HashMap<String, Object>();

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");
		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(response.toString());

		int j = 0;
		for (int i = 0; i < 6; i++) {

			j = i + 1;
			lv_hs.put("N" + Integer.toString(i), jsonObject.get("drwtNo" + j));

		}

		// 회차 정보 입력
		lv_hs.put("drwNo", num);

		System.out.println(lv_hs);
		return lv_hs;
	}

	// HTTP POST request
	private void sendPost() throws Exception {

		String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());

	}

}