package alpha.filter;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class SaveFrange {

	private static SqlSessionFactory sqlMapper = null;

	static {
		try {

			String resource = "config/spring/context-mybatis.xml";
			Reader reader = Resources.getResourceAsReader(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(reader);
		} catch (IOException e) {
			e.printStackTrace();

		}

	}

	public void saveFrange(String sID, int sNum, int eNum) throws Exception {

		try {

			SqlSession session = sqlMapper.openSession();

			Map<String, Integer> smap = new HashMap<String, Integer>();
			Map<String, Object> imap = new HashMap<String, Object>();
			smap.put("start", sNum);
			smap.put("end", eNum);

			List<Map<String, Object>> list = session.selectList("sample.selectNumbers", smap);

			int cnt = 0;

			session.delete("sample.deleteNumbers", sID);
			session.commit();

			System.out.println(list);

			for (int i = 1; i < 7; i++) {
				for (int j = 1; j < 46; j++) {

					cnt = 0;

					for (int q = 0; q < list.size(); q++) {
						if (Integer.parseInt(list.get(q).get("no" + i).toString()) == j)
							cnt++;
					}
					imap.put("sID", sID);
					imap.put("scope", i);
					imap.put("num", j);
					imap.put("cnt", cnt);

					session.insert("sample.insertNumbers", imap);
				}

			}

			session.commit();
			session.close();

			System.out.println("the end");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
